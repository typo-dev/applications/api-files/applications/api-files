<?php

declare(strict_types=1);

namespace App\Domain\Entity;

final readonly class OptimizedFile
{
    public function __construct(
        private string $id,
        private string $mimetype,
        private int $size,
        private \DateTime $createdAt
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getMimetype(): string
    {
        return $this->mimetype;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }
}
