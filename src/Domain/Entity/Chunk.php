<?php

namespace App\Domain\Entity;

class Chunk
{
    public function __construct(
        private readonly string $id,
        private readonly int $number,
        private readonly \DateTime $uploadedAt,
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function getUploadedAt(): \DateTime
    {
        return $this->uploadedAt;
    }
}
