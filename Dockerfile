FROM registry.gitlab.com/typo-indus/docker/php-symfony-apache/8.3:psql

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
      libfreetype6-dev \
      libpng-dev \
      libjpeg62-turbo-dev \
      libjpeg-dev \
      libwebp-dev \
      supervisor \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-configure gd \
      --prefix=/usr \
      --with-jpeg \
      --with-webp \
      --with-freetype \
    && docker-php-ext-install gd

RUN pecl install -o -f redis \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable redis

WORKDIR /app

COPY . .

RUN mkdir -p var/log && chown -R www-data: /app

COPY .docker/supervisor/messenger-worker.conf /etc/supervisor/conf.d/messenger-worker.conf

ENTRYPOINT ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf", "-n"]

