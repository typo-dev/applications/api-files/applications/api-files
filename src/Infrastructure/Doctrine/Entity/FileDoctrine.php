<?php

namespace App\Infrastructure\Doctrine\Entity;

use App\Domain\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 't_file')]
class FileDoctrine
{
    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 36, unique: true)]
    private string $id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $filename;

    #[ORM\Column(type: 'string', length: 127)]
    private string $mimeType;

    #[ORM\Column(type: 'string', length: 127)]
    private string $path;

    #[ORM\Column(type: 'integer')]
    private int $size;

    #[ORM\Column(type: 'datetime')]
    private \DateTime $uploadedAt;

    #[ORM\OneToMany(targetEntity: OptimizedFileDoctrine::class, mappedBy: 'file')]
    private Collection $optimizedFiles;

    public function __construct(
        string $id,
        string $filename,
        string $path,
        string $mimeType,
        int $size,
        \DateTime $uploadedAt
    ) {
        $this->id = $id;
        $this->filename = $filename;
        $this->path = $path;
        $this->mimeType = $mimeType;
        $this->size = $size;
        $this->uploadedAt = $uploadedAt;
        $this->optimizedFiles = new ArrayCollection();
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function toFile(): File
    {
        return new File(
            $this->id,
            $this->filename,
            $this->mimeType,
            $this->size,
            $this->uploadedAt,
            array_map(function (OptimizedFileDoctrine $optimizedFile) {
                return $optimizedFile->toOptimizedFile();
            }, $this->optimizedFiles->toArray())
        );
    }
}
