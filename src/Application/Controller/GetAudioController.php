<?php

namespace App\Application\Controller;

use App\Domain\UseCase\GetAudioOriginalUseCase;
use App\Domain\UseCase\GetFileContentUseCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

class GetAudioController
{
    public function __construct(
        private GetAudioOriginalUseCase $getAudioOriginalUseCase,
        private GetFileContentUseCase $getFileContentUseCase,
    ) {
    }

    #[Route("/audio/original/{id}/{filename}", name: "app_get_audio_original", methods: ["GET"])]
    public function getAudioOriginalAction(string $id, string $filename, Request $request): Response
    {
        $file = $this->getAudioOriginalUseCase->executer($id, $filename);

        $fileContent = $this->getFileContentUseCase->executer($id);

        $fileSize = $file->getSize();

        $range = $request->get("range");
        if ($range) {
            preg_match('/bytes=(\d+)-(\d+)?/', $range, $matches);
            $start = (int)$matches[1];
            $end = isset($matches[2]) ? (int)$matches[2] : $fileSize - 1;

            if ($start >= $fileSize || $end >= $fileSize) {
                return new Response('Requested Range Not Satisfiable', 416);
            }

            $length = $end - $start + 1;
            if ($length < 1) {
                return new Response('Requested Range Not Satisfiable', 416);
            }
            fseek($fileContent, $start);

            return new StreamedResponse(function () use ($fileContent, $length) {
                $data = fread($fileContent, $length);
                echo $data;
                fclose($fileContent);
            }, Response::HTTP_PARTIAL_CONTENT, [
                "Content-Type" => $file->getMimeType(),
                "Content-Length" => $length,
                "Content-Range" => "bytes $start-$end/$fileSize",
                "Content-Disposition" => "inline; filename=\"" . $file->getFilename() . "\"",
                "Accept-Ranges" => "bytes",
            ]);
        }

        return new StreamedResponse(function () use ($fileContent) {
            $chunkSize = 1024 * 1024; // 1 Mo
            while (!feof($fileContent)) {
                echo fread($fileContent, $chunkSize);
                flush(); // Force l'envoi des données au client
            }
            fclose($fileContent);
        }, Response::HTTP_OK, [
            "Content-Type" => $file->getMimeType(),
            "Content-Length" => $fileSize,
            "Content-Disposition" => "inline; filename=\"" . $file->getFilename() . "\"",
            "Accept-Ranges" => "bytes",
        ]);
    }
}
