<?php

namespace App\Domain\UseCase;

use App\Domain\Exception\FileNotFoundException;
use App\Domain\Exception\FilePersistException;
use App\Domain\Port\FilePersist;

class GetFileContentInNewFormatUseCase
{
    public function __construct(
        private FilePersist $filePersist,
    ) {
    }

    /**
     * @throws FilePersistException
     * @throws FileNotFoundException
     */
    public function executer(string $id, string $format, int $width): string
    {
        return $this->filePersist->getFileContentInNewFormat($id, $format, $width);
    }
}
