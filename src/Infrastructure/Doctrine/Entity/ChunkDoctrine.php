<?php

namespace App\Infrastructure\Doctrine\Entity;

use App\Domain\Entity\Chunk;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 't_chunk')]
class ChunkDoctrine
{
    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 36, unique: true)]
    private string $id;

    #[ORM\Column(type: 'integer')]
    private int $number;

    #[ORM\Column(type: 'datetime')]
    private \DateTime $uploadedAt;

    #[ORM\Column(type: 'string', length: 127)]
    private string $path;

    #[ORM\ManyToOne(targetEntity: UploadingFileDoctrine::class, inversedBy: 'chunks')]
    private UploadingFileDoctrine $uploadingFile;

    public function __construct(
        string $id,
        int $number,
        \DateTime $uploadedAt,
        string $path
    ) {
        $this->id = $id;
        $this->number = $number;
        $this->uploadedAt = $uploadedAt;
        $this->path = $path;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function toChunk(): Chunk
    {
        return new Chunk(
            $this->id,
            $this->number,
            $this->uploadedAt
        );
    }
}
