<?php

namespace App\Application\Controller;

use App\Domain\UseCase\GetFileContentInNewFormatUseCase;
use App\Domain\UseCase\GetFileContentUseCase;
use App\Domain\UseCase\GetImageOptimizedUseCase;
use App\Domain\UseCase\GetImageOriginalUseCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

class GetImageController
{
    public function __construct(
        private GetImageOriginalUseCase $getImageOriginalUseCase,
        private GetFileContentUseCase $getFileContentUseCase,
        private GetFileContentInNewFormatUseCase $getFileContentInNewFormatUseCase,
        private GetImageOptimizedUseCase $getImageOptimizedUseCase,
    ) {
    }

    #[Route("/image/original/{id}/{filename}", name: "app_get_images", methods: ["GET"])]
    public function getImagesAction(string $id, string $filename): StreamedResponse
    {
        $file = $this->getImageOriginalUseCase->executer($id, $filename);

        $fileContent = $this->getFileContentUseCase->executer($id);

        return new StreamedResponse(function () use ($fileContent) {
            fpassthru($fileContent);
        }, Response::HTTP_OK, [
            "Content-Type" => $file->getMimeType(),
            "Content-Length" => $file->getSize(),
            "Content-Disposition" => "inline; filename=\"" . $file->getFilename() . "\"",
        ]);
    }

    #[Route("/image/optimize/{id}/{format}/{width}/{filename}", name: "app_get_images_optimized", methods: ["GET"])]
    public function getImageOptimizedAction(string $id, string $format, int $width, string $filename): Response
    {
        $file = $this->getImageOptimizedUseCase->executer($id, $filename, $format);

        $fileContent = $this->getFileContentInNewFormatUseCase->executer($id, $format, $width);

        return new Response($fileContent, Response::HTTP_OK, [
            "Content-Type" => 'image/' . $format,
            "Content-Length" => strlen($fileContent),
            "Content-Disposition" => "inline; filename=\"" . $filename . "\"",
        ]);
    }
}
