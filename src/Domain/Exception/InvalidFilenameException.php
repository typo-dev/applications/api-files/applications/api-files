<?php

namespace App\Domain\Exception;

use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class InvalidFilenameException extends UnprocessableEntityHttpException
{
}
