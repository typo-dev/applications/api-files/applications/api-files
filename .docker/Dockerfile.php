FROM registry.gitlab.com/typo-indus/docker/php-symfony-apache/8.3:psql-debug

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
      libfreetype6-dev \
      libpng-dev \
      libjpeg62-turbo-dev \
      libjpeg-dev \
      libwebp-dev \
      libavif-dev libaom-dev libdav1d-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-configure gd \
      --prefix=/usr \
      --with-jpeg \
      --with-webp \
      --with-freetype \
      --with-avif \
    && docker-php-ext-install gd

RUN pecl install -o -f redis \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable redis