# API FILES

```mermaid
sequenceDiagram
    participant Frontend as Frontend
    participant ApiFiles as ApiFiles

    Frontend->>Frontend: Sélectionne le fichier à uploader
    Frontend->>ApiFiles: POST /upload/init
    activate ApiFiles
    ApiFiles-->>Frontend: Renvoit de l'identifiant unique
    deactivate ApiFiles

    loop Pour chaque chunk du fichier
        Frontend->>Frontend: Sépare le fichier en chunks
        Frontend->>ApiFiles: POST /upload/chunk avec l'identifiant unique
        activate ApiFiles

        ApiFiles-->>Frontend: Réception du chunk
        deactivate ApiFiles

        alt Chunk valide
            Frontend-->>Frontend: Passe au chunk suivant
        else Chunk invalide
            Frontend->>ApiFiles: Demande de retry pour le chunk avec l'identifiant unique
            activate ApiFiles

            ApiFiles-->>Frontend: Retry accordé
            deactivate ApiFiles
        end
    end

    Frontend->>ApiFiles: POST /upload/complete avec l'identifiant unique
    activate ApiFiles
    ApiFiles-->>Frontend: Confirmation de la réception complète avec l'identifiant unique
    deactivate ApiFiles

    Frontend->>ApiFiles: POST /upload/verify avec l'identifiant unique et la somme de contrôle
    activate ApiFiles
    ApiFiles-->>Frontend: Résultat de la vérification de l'intégrité
    deactivate ApiFiles

    Frontend->>ApiFiles: Optionnel - POST /upload/merge avec l'identifiant unique
    activate ApiFiles
    ApiFiles-->>Frontend: Optionnel - Confirmation de la fusion avec l'identifiant unique
    deactivate ApiFiles

```

A faire :

- route qui liste tous les fichiers de la bdd
- batch qui vérifie que tous les fichiers uploadés sont bien en bdd et qui les supprime sinon
- batch qui supprime les uploading file de plus de 48h et les chunks
- Vérifier dans use case que fichier a extension
- Ajouter format au optimizedFile
