<?php

namespace App\Domain\Exception;

use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class InvalidChunckedFileInputException extends UnprocessableEntityHttpException
{
}
