<?php

namespace App\Infrastructure\Provider;

use App\Domain\Entity\File;
use App\Domain\Exception\FileNotFoundException;
use App\Domain\Exception\FilePersistException;
use App\Domain\Port\FilePersist;
use App\Infrastructure\Doctrine\Entity\FileDoctrine;
use Doctrine\ORM\EntityManagerInterface;
use League\Flysystem\FilesystemException;
use League\Flysystem\FilesystemOperator;

final readonly class FilePersistProvider implements FilePersist
{
    public function __construct(
        private FilesystemOperator $defaultFilesystem,
        private EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * @throws FilePersistException
     */
    public function uploadFile(File $file, string $decodedContent): void
    {
        $path = $this->calculatePath($file);

        try {
            $this->defaultFilesystem->write($path, $decodedContent);
        } catch (FilesystemException $e) {
            throw new FilePersistException(
                'An error occurred while uploading the file.',
                $e,
                $e->getCode(),
            );
        }

        $fileDoctrine = new FileDoctrine(
            $file->getId(),
            $file->getFilename(),
            $path,
            $file->getMimeType(),
            $file->getSize(),
            $file->getUploadedAt()
        );

        $this->entityManager->persist($fileDoctrine);
        $this->entityManager->flush();
    }

    private function calculatePath(File $file): string
    {
        $id = $file->getId();
        $fileInfo = pathinfo($file->getFilename());

        if (empty($fileInfo['extension'])) {
            throw new FilePersistException('Filename should have a file extension.');
        }
        $extension = $fileInfo['extension'];

        return $file->getUploadedAt()->format('Y-m-d') . '/' . $id . '.' . $extension;
    }

    public function getFile(string $id): File
    {
        $file = $this->getFileDoctrineById($id);
        return $file->toFile();
    }

    /**
     * @return resource
     */
    public function getFileContent(string $id)
    {
        $file = $this->getFileDoctrineById($id);
        try {
            return $this->defaultFilesystem->readStream($file->getPath());
        } catch (FilesystemException $e) {
            throw new FilePersistException('Impossible to read the file.', $e);
        }
    }

    public function getFileDoctrineById(string $id): FileDoctrine
    {
        /** @var FileDoctrine|null $file */
        $file = $this->entityManager->getRepository(FileDoctrine::class)->find($id);

        if (!$file) {
            throw new FileNotFoundException('File not found.');
        }
        return $file;
    }

    public function getFileContentInNewFormat(string $id, string $format, int $newWidth): string
    {
        $file = $this->getFileDoctrineById($id);
        try {
            $content = $this->defaultFilesystem->read($file->getPath());
            $image = imagecreatefromstring($content);
            if (!$image) {
                throw new FilePersistException('Impossible to read the file.');
            }
            $width = imagesx($image);
            $height = imagesy($image);

            if ($width > $newWidth) {
                $height = $newWidth * $height / $width;
                $width = $newWidth;
                $image = imagescale($image, $width, $height);
                if (!$image) {
                    throw new FilePersistException('Impossible to read the file.');
                }
            }

            ob_start();
            switch ($format) {
                case 'jpg':
                    imagejpeg($image, null, 80);
                    break;
                case 'png':
                    imagepng($image, null, 80);
                    break;
                case 'webp':
                    imagewebp($image, null, 80);
                    break;
                case 'avif':
                    imageavif($image, null, 80);
                    break;
            }
            $newImage = ob_get_clean();
            if (!$newImage) {
                throw new FilePersistException('Impossible to read the file.');
            }
            return $newImage;
        } catch (FilesystemException $e) {
            throw new FilePersistException('Impossible to read the file.', $e);
        }
    }
}
