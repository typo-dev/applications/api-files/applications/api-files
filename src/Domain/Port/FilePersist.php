<?php

namespace App\Domain\Port;

use App\Domain\Entity\File;
use App\Domain\Exception\FileNotFoundException;
use App\Domain\Exception\FilePersistException;

interface FilePersist
{
    /**
     * @throws FilePersistException
     */
    public function uploadFile(File $file, string $decodedContent): void;

    /**
     * @throws FileNotFoundException
     */
    public function getFile(string $id): File;

    /**
     * @throws FilePersistException
     * @throws FileNotFoundException
     * @return resource
     */
    public function getFileContent(string $id);

    /**
     * @throws FilePersistException
     * @throws FileNotFoundException
     */
    public function getFileContentInNewFormat(string $id, string $format, int $newWidth): string;
}
