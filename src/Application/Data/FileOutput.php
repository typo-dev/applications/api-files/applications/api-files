<?php

namespace App\Application\Data;

class FileOutput
{
    public function __construct(
        private readonly string $uniqid,
        private readonly string $filename,
        private readonly string $mimetype,
        private readonly int $size,
        private readonly \DateTime $uploadedAt,
    ) {
    }
}
