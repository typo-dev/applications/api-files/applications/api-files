<?php

namespace App\Domain\UseCase;

use App\Domain\Entity\File;
use App\Domain\Entity\FileType;
use App\Domain\Exception\FileNotFoundException;
use App\Domain\Port\FilePersist;

class GetImageOptimizedUseCase
{
    public function __construct(
        private readonly FilePersist $filePersist,
    ) {
    }

    /**
     * @throws FileNotFoundException
     */
    public function executer(string $id, string $filename, string $format): File
    {
        if (!in_array($format, ['webp', 'avif', 'jpg', 'png'])) {
            throw new FileNotFoundException("The format should be 'jpg', 'png', 'webp' or 'avif'.");
        }

        $file = $this->filePersist->getFile($id);

        $filePathInfo = pathinfo($file->getFilename());

        $goodFileName = $filePathInfo['filename'] . '.' . $format;
        if (
            $filename !== $goodFileName
        ) {
            throw new FileNotFoundException("The file with the id '{$id}' should have the filename '{$goodFileName}'.");
        }

        if ($file->getType() !== FileType::Image) {
            throw new FileNotFoundException("The file with the id '{$id}' is not an image.");
        }

        return $file;
    }
}
