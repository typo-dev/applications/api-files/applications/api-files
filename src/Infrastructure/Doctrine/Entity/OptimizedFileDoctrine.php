<?php

namespace App\Infrastructure\Doctrine\Entity;

use App\Domain\Entity\OptimizedFile;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 't_optimized_file')]
class OptimizedFileDoctrine
{
    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 36, unique: true)]
    private string $id;

    #[ORM\Column(type: 'string', length: 127)]
    private string $mimeType;

    #[ORM\Column(type: 'integer')]
    private int $size;

    #[ORM\Column(type: 'datetime')]
    private \DateTime $createdAt;

    #[ORM\ManyToOne(targetEntity: FileDoctrine::class, inversedBy: 'optimizedFiles')]
    private FileDoctrine $file;

    public function toOptimizedFile(): OptimizedFile
    {
        return new OptimizedFile(
            $this->id,
            $this->mimeType,
            $this->size,
            $this->createdAt
        );
    }
}
