<?php

namespace App\Infrastructure\Provider;

use App\Domain\Port\GenerateUniqid;
use Ramsey\Uuid\Uuid;

class GenerateUniqidProvider implements GenerateUniqid
{
    public function generate(): string
    {
        $uuid = Uuid::uuid4();

        return $uuid->toString();
    }
}
