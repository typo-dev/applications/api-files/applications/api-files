
DOCKER_COMP = docker-compose
API_FILES = $(DOCKER_COMP) exec -u www-data api-files
SYMFONY = $(API_FILES) bin/console

help: ## Afficher ce message d'aide
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
.PHONY: help

lint-yaml: ## Lance le linter yaml
	@docker run --pull always --rm $$(tty -s && echo "-it" || echo) -v $(PWD):/data cytopia/yamllint:latest .
.PHONY: lint-yaml

lint-markdown: ## Lance le linter markdown
	@docker run --pull always --rm -v $(PWD):/app -w /app registry.gitlab.com/typo-indus/docker/markdownlint:main /bin/sh -c 'markdownlint "README.md"'
.PHONY: lint-markdown

lint-dockerfile: ## Lance le linter dockerfile
	@docker run --pull always --rm -v $(PWD):/app -w /app hadolint/hadolint:latest-debian hadolint Dockerfile
.PHONY: lint-dockerfile

phpcs: ## Lance phpcs
	docker run --pull always --init -it --rm -v "$(PWD):/app" -w /app registry.gitlab.com/typo-indus/docker/phpqa:main /root/.composer/vendor/bin/phpcs -v --standard=.phpcs.xml
.PHONY: phpcs

phpcs-fix: ## Lance la correction auto des erreurs phpcs
	docker run --pull always --init -it --rm -v "$(PWD):/app" -w /app registry.gitlab.com/typo-indus/docker/phpqa:main /root/.composer/vendor/bin/phpcbf -v --standard=.phpcs.xml
.PHONY: phpcs-fix

phpcsfixer: ## Lance la correction auto des erreurs phpcs
	docker run --pull always --init -it --rm -v "$(PWD):/app" -w /app registry.gitlab.com/typo-indus/docker/phpqa:main /root/.composer/vendor/bin/php-cs-fixer fix --dry-run --diff --config=.php-cs-fixer.dist.php --no-interaction
.PHONY: phpcsfixer

phpcsfixer-fix: ## Lance la correction auto des erreurs phpcs
	docker run --pull always --init -it --rm -v "$(PWD):/app" -w /app registry.gitlab.com/typo-indus/docker/phpqa:main /root/.composer/vendor/bin/php-cs-fixer fix --config=.php-cs-fixer.dist.php --no-interaction
.PHONY: phpcsfixer-fix

phpstan:  ## Lance phpstan
	docker run --pull always --init -it --rm -v "$(PWD):/app" -w /app registry.gitlab.com/typo-indus/docker/phpqa:main /root/.composer/vendor/bin/phpstan analyse -c phpstan.neon
.PHONY: phpstan

phpmd: ## Lance PHPMD
	docker run --pull always --init -it --rm -v "$(PWD):/app" -w /app registry.gitlab.com/typo-indus/docker/phpqa:main /root/.composer/vendor/bin/phpmd src text ./phpmd.xml
.PHONY: phpmd

deptrac:  ## Lance deptrac
	docker run --pull always --init -it --rm -v "$(PWD):/app" -w /app registry.gitlab.com/typo-indus/docker/phpqa:main /root/.composer/vendor/bin/deptrac --no-interaction --ansi --report-uncovered
.PHONY: deptrac

php-lint: phpcs phpcsfixer phpstan phpmd deptrac ## Lance linter php
.PHONY: php-lint

test-unit:
	@$(DOCKER_COMP) up -d
	@$(SYMFONY) c:c --env=test
	@$(API_FILES) bash -c "XDEBUG_MODE=coverage php bin/phpunit --filter Domain --configuration phpunit-unit.xml.dist"
.PHONY: test-unit

test-int:
	@$(DOCKER_COMP) up -d
	@$(SYMFONY) c:c --env=test
	@$(SYMFONY) doctrine:database:drop --force --env=test --if-exists
	@$(SYMFONY) doctrine:database:create --env=test
	@$(SYMFONY) doctrine:migrations:migrate -n --env=test
	@$(API_FILES) bash -c "XDEBUG_MODE=coverage php bin/phpunit --filter Infrastructure --configuration phpunit-int.xml.dist"
.PHONY: test-int

test-fonc:
	@$(DOCKER_COMP) up -d
	@$(SYMFONY) c:c --env=test
	@$(SYMFONY) doctrine:database:drop --force --env=test --if-exists
	@$(SYMFONY) doctrine:database:create --env=test
	@$(SYMFONY) doctrine:migrations:migrate -n --env=test
	@$(API_FILES) bash -c "XDEBUG_MODE=coverage php bin/phpunit --filter Application --configuration phpunit-fonc.xml.dist"
.PHONY: test-fonc

test:
	@$(DOCKER_COMP) up -d
	@$(SYMFONY) c:c --env=test
	@$(SYMFONY) doctrine:database:drop --force --env=test --if-exists
	@$(SYMFONY) doctrine:database:create --env=test
	@$(SYMFONY) doctrine:migrations:migrate -n --env=test
	@$(API_FILES) bash -c "XDEBUG_MODE=coverage php bin/phpunit --configuration phpunit.xml.dist"
.PHONY: test

init-test-database:
	@$(DOCKER_COMP) up -d
	@$(SYMFONY) c:c --env=test
	@$(SYMFONY) doctrine:database:drop --force --env=test --if-exists
	@$(SYMFONY) doctrine:database:create --env=test
	@$(SYMFONY) doctrine:migrations:migrate -n --env=test
.PHONY: init-test-database

infection:
	@$(DOCKER_COMP) up -d
	@$(SYMFONY) c:c --env=test
	@$(SYMFONY) doctrine:database:drop --force --env=test --if-exists
	@$(SYMFONY) doctrine:database:create --env=test
	@$(SYMFONY) doctrine:migrations:migrate -n --env=test
	docker-compose exec -u www-data api-files composer global require infection/infection
	docker-compose exec -u www-data api-files bash -c "XDEBUG_MODE=coverage php /home/www-data/.composer/vendor/bin/infection --threads=4 --logger-html='mutation-rapport.html'"