<?php

declare(strict_types=1);

namespace App\Domain\Input;

final class ChunckedUploadedFileInput
{
    public function __construct(
        private readonly string|null $id,
        private readonly string $filename,
        private readonly string $chunk,
        private readonly int $totalChunks,
        private readonly int $currentChunk,
    ) {
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function getChunk(): string
    {
        return $this->chunk;
    }

    public function getTotalChunks(): int
    {
        return $this->totalChunks;
    }

    public function getCurrentChunk(): int
    {
        return $this->currentChunk;
    }
}
