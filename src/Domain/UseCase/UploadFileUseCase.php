<?php

declare(strict_types=1);

namespace App\Domain\UseCase;

use App\Domain\Entity\File;
use App\Domain\Exception\FilePersistException;
use App\Domain\Exception\InvalidFileInputException;
use App\Domain\Exception\InvalidFilenameException;
use App\Domain\Input\UploadedFileInput;
use App\Domain\Port\FilePersist;
use App\Domain\Port\GenerateUniqid;
use App\Domain\Service\FileVerificationService;

final class UploadFileUseCase
{
    public const CONTENT_MAX_SIZE = 5 * 1024 * 1024;

    public function __construct(
        private readonly GenerateUniqid $generateUniqid,
        private readonly FilePersist $filePersist,
        private readonly FileVerificationService $fileVerificationService,
    ) {
    }

    /**
     * @throws InvalidFileInputException
     * @throws InvalidFilenameException
     * @throws FilePersistException
     */
    public function execute(UploadedFileInput $uploadFileInput, \DateTime $uploadedAt): File
    {
        $this->fileVerificationService->validateFilename($uploadFileInput->getFilename());

        $decodedContent = $this->validateContentAndGetDecodedContent($uploadFileInput);

        $file = $this->createFile($uploadFileInput, $decodedContent, $uploadedAt);

        $this->filePersist->uploadFile($file, $decodedContent);

        return $file;
    }

    /**
     * @throws InvalidFileInputException
     */
    private function validateContentAndGetDecodedContent(UploadedFileInput $uploadFileInput): string
    {
        $contentEncoded = $uploadFileInput->getContent();

        if (!$contentEncoded) {
            throw new InvalidFileInputException('The content cannot be empty.');
        }

        $contentDecoded = base64_decode($contentEncoded);
        if (!$contentDecoded) {
            throw new InvalidFileInputException('The content should be base64 encoded.');
        }

        if (strlen($contentDecoded) > self::CONTENT_MAX_SIZE) {
            throw new InvalidFileInputException('The content cannot be bigger than 5Mo.');
        }

        return $contentDecoded;
    }

    private function createFile(UploadedFileInput $uploadFileInput, string $decodedContent, \DateTime $uploadedAt): File
    {
        return new File(
            $this->generateUniqid->generate(),
            $uploadFileInput->getFilename(),
            $this->getMimetype($decodedContent),
            strlen($decodedContent),
            $uploadedAt
        );
    }

    private function getMimetype(string $content): string
    {
        $finfo = finfo_open();

        if ($finfo === false) {
            return 'application/octet-stream';
        }

        $mimetype = finfo_buffer($finfo, $content, FILEINFO_MIME_TYPE);
        if ($mimetype === false) {
            return 'application/octet-stream';
        }
        return $mimetype;
    }
}
