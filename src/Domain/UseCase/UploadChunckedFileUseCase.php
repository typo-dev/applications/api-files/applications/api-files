<?php

namespace App\Domain\UseCase;

use App\Domain\Entity\Chunk;
use App\Domain\Entity\UploadingFile;
use App\Domain\Exception\InvalidChunckedFileInputException;
use App\Domain\Input\ChunckedUploadedFileInput;
use App\Domain\Port\GenerateUniqid;
use App\Domain\Port\UploadingFilePersist;
use App\Domain\Service\FileVerificationService;

class UploadChunckedFileUseCase
{
    public const CHUNK_MAX_SIZE = 1024 * 1024;


    public function __construct(
        private readonly FileVerificationService $fileVerificationService,
        private readonly GenerateUniqid $generateUniqid,
        private readonly UploadingFilePersist $uploadingFilePersist,
    ) {
    }

    public function execute(ChunckedUploadedFileInput $chunckedUploadedFileInput, \DateTime $uploadedAt): UploadingFile
    {
        $this->fileVerificationService->validateFilename($chunckedUploadedFileInput->getFilename());

        $this->validateChunk($chunckedUploadedFileInput->getChunk());

        if ($chunckedUploadedFileInput->getId() === null) {
            $uploadingFile = $this->initUploadingFile($chunckedUploadedFileInput, $uploadedAt);
        } else {
            $uploadingFile = $this->uploadingFilePersist->getById($chunckedUploadedFileInput->getId());
        }

        return $this->uploadChunk($uploadingFile, $chunckedUploadedFileInput, $uploadedAt);
    }

    /**
     * @throws InvalidChunckedFileInputException
     */
    private function validateChunk(string $getChunk): void
    {
        if (strlen($getChunk) > self::CHUNK_MAX_SIZE) {
            throw new InvalidChunckedFileInputException(
                'Chunk should be less than ' . self::CHUNK_MAX_SIZE . ' bytes'
            );
        }
    }

    private function initUploadingFile(
        ChunckedUploadedFileInput $chunckedUploadedFileInput,
        \DateTime $createdAt
    ): UploadingFile {
        $uploadingFile = new UploadingFile(
            $this->generateUniqid->generate(),
            $chunckedUploadedFileInput->getFilename(),
            $createdAt,
            $chunckedUploadedFileInput->getTotalChunks()
        );

        $this->uploadingFilePersist->createUploadingFile($uploadingFile);

        return $uploadingFile;
    }

    private function uploadChunk(
        UploadingFile $uploadingFile,
        ChunckedUploadedFileInput $chunckedUploadedFileInput,
        \DateTime $uploadedAt
    ): UploadingFile {
        $chunk = new Chunk(
            $this->generateUniqid->generate(),
            $chunckedUploadedFileInput->getCurrentChunk(),
            $uploadedAt
        );

        return $this->uploadingFilePersist->uploadChunk(
            $uploadingFile,
            $chunk,
            $chunckedUploadedFileInput->getChunk()
        );
    }
}
