<?php

namespace App\Application\Mapper;

use App\Application\Data\FileOutput;
use App\Domain\Entity\File;

class FileOutputMapper
{
    public function toFileOutput(File $file): FileOutput
    {
        return new FileOutput(
            $file->getId(),
            $file->getFilename(),
            $file->getMimeType(),
            $file->getSize(),
            $file->getUploadedAt()
        );
    }
}
