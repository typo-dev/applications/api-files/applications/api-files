<?php

declare(strict_types=1);

namespace App\Domain\Entity;

final readonly class File
{
    /**
     * @param array<OptimizedFile> $optimizedFiles
     */
    public function __construct(
        private string $id,
        private string $filename,
        private string $mimetype,
        private int $size,
        private \DateTime $uploadedAt,
        private array $optimizedFiles = [],
    ) {
    }

    public function getType(): FileType
    {
        return match ($this->mimetype) {
            'image/gif', 'image/bmp', 'image/jpeg',
            'image/jpg', 'image/png', 'image/webp', 'image/avif' => FileType::Image,
            'audio/mpeg', 'audio/ogg', 'audio/vnd.wav', 'audio/flac' => FileType::Audio,
            default => FileType::Other,
        };
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function getMimeType(): string
    {
        return $this->mimetype;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function getUploadedAt(): \DateTime
    {
        return $this->uploadedAt;
    }

    /**
     * @return array<OptimizedFile>
     */
    public function getOptimizedFiles(): array
    {
        return $this->optimizedFiles;
    }
}
