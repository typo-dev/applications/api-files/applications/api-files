<?php

namespace App\Infrastructure\Provider;

use App\Domain\Entity\Chunk;
use App\Domain\Entity\File;
use App\Domain\Entity\UploadingFile;
use App\Domain\Exception\FilePersistException;
use App\Domain\Port\UploadingFilePersist;
use App\Infrastructure\Doctrine\Entity\ChunkDoctrine;
use App\Infrastructure\Doctrine\Entity\FileDoctrine;
use App\Infrastructure\Doctrine\Entity\UploadingFileDoctrine;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use League\Flysystem\FilesystemException;
use League\Flysystem\FilesystemOperator;
use Psr\Log\LoggerInterface;

class UploadingFilePersistProvider implements UploadingFilePersist
{
    public function __construct(
        private FilesystemOperator $defaultFilesystem,
        private EntityManagerInterface $entityManager,
        private LoggerInterface $logger
    ) {
    }

    public function createUploadingFile(UploadingFile $uploadingFile): void
    {
        $uploadingFileDoctrine = new UploadingFileDoctrine(
            $uploadingFile->getId(),
            $uploadingFile->getFilename(),
            $uploadingFile->getStartedAt(),
            $uploadingFile->getTotalChunks()
        );

        $this->entityManager->persist($uploadingFileDoctrine);
        $this->entityManager->flush();
    }

    public function getById(string $id): UploadingFile
    {
        $uploadingFileDoctrine = $this->getFileDoctrineById($id);

        return $uploadingFileDoctrine->toUploadingFile();
    }

    public function uploadChunk(UploadingFile $uploadingFile, Chunk $chunk, string $content): UploadingFile
    {
        $id = $uploadingFile->getId();

        $uploadingFileDoctrine = $this->getFileDoctrineById($id);

        $path = $this->calculatePath($id, $chunk);

        try {
            $this->defaultFilesystem->write($path, $content);
        } catch (FilesystemException $e) {
            throw new FilePersistException(
                'An error occurred while uploading the chunk.',
                $e,
                $e->getCode(),
            );
        }

        $chunkDoctrine = new ChunkDoctrine(
            $chunk->getId(),
            $chunk->getNumber(),
            $chunk->getUploadedAt(),
            $path,
        );

        $uploadingFileDoctrine->addChunk($chunkDoctrine);

        $this->entityManager->persist($chunkDoctrine);
        $this->entityManager->persist($uploadingFileDoctrine);

        $this->entityManager->flush();

        return $uploadingFileDoctrine->toUploadingFile();
    }

    public function mergedChunkAndCreateFile(string $idUploadingFile, string $newId): File
    {
        $uploadingFileDoctrine = $this->getFileDoctrineById($idUploadingFile);

        $chunks = $uploadingFileDoctrine->getChunks();

        $out = fopen('php://temp', 'wb+');
        if (!$out) {
            throw new \RuntimeException('Unable to open output file');
        }

        try {
            /**
             * @var ChunkDoctrine $chunk
             */
            foreach ($chunks as $chunk) {
                $content = $this->defaultFilesystem->readStream($chunk->getPath());
                while ($buff = fread($content, 4096)) {
                    fwrite($out, $buff);
                }
            }

            rewind($out);

            $finalFilePath = $this->calculateFilePath(
                $newId,
                $uploadingFileDoctrine->getFilename(),
                $uploadingFileDoctrine->getStartedAt()
            );

            $this->defaultFilesystem->writeStream($finalFilePath, $out);
            fclose($out);

            $mimeType = $this->defaultFilesystem->mimeType($finalFilePath);
            $size = $this->defaultFilesystem->fileSize($finalFilePath);
        } catch (FilesystemException $e) {
            throw new FilePersistException('Impossible to merge the files', $e);
        }

        if (!$mimeType || !$size) {
            throw new FilePersistException('');
        }

        $fileDoctrine = new FileDoctrine(
            $newId,
            $uploadingFileDoctrine->getFilename(),
            $finalFilePath,
            $mimeType,
            $size,
            $uploadingFileDoctrine->getStartedAt(),
        );

        $this->entityManager->persist($fileDoctrine);
        $this->entityManager->flush();

        /**
         * @var ChunkDoctrine $chunk
         */
        foreach ($chunks as $chunk) {
            try {
                $this->entityManager->remove($chunk);
                $this->defaultFilesystem->delete($chunk->getPath());
            } catch (FilesystemException $e) {
                $this->logger->warning('Impossible to delete the file ' . $chunk->getPath(), [$e]);
            }
        }
        try {
            $this->defaultFilesystem->deleteDirectory('chunk/' . $idUploadingFile);
        } catch (FilesystemException $e) {
            $this->logger->warning('Impossible to delete directory chunk/' . $idUploadingFile, [$e]);
        }
        $this->entityManager->remove($uploadingFileDoctrine);
        $this->entityManager->flush();

        return $fileDoctrine->toFile();
    }

    private function calculatePath(string $id, Chunk $chunk): string
    {
        return 'chunk/' . $id . '/' . str_pad((string)$chunk->getNumber(), 10, '0', STR_PAD_LEFT);
    }

    private function getFileDoctrineById(string $id): UploadingFileDoctrine
    {
        /** @var UploadingFileDoctrine|null $uploadingFileDoctrine */
        $uploadingFileDoctrine = $this->entityManager->getRepository(UploadingFile::class)->find($id);

        if (null === $uploadingFileDoctrine) {
            throw new EntityNotFoundException(UploadingFile::class, $id);
        }
        return $uploadingFileDoctrine;
    }

    // COPY /!\
    private function calculateFilePath(string $id, string $filename, \DateTime $uploadedAt): string
    {
        $fileInfo = pathinfo($filename);

        if (empty($fileInfo['extension'])) {
            throw new FilePersistException('Filename should have a file extension.');
        }
        $extension = $fileInfo['extension'];

        return $uploadedAt->format('Y-m-d') . '/' . $id . '.' . $extension;
    }
}
