<?php

namespace App\Domain\UseCase;

use App\Domain\Entity\File;
use App\Domain\Entity\FileType;
use App\Domain\Exception\FileNotFoundException;
use App\Domain\Port\FilePersist;

class GetImageOriginalUseCase
{
    public function __construct(
        private FilePersist $filePersist,
    ) {
    }

    /**
     * @throws FileNotFoundException
     */
    public function executer(string $id, string $filename): File
    {
        $file = $this->filePersist->getFile($id);

        if ($file->getFilename() !== $filename) {
            throw new FileNotFoundException(
                "The file with the id '{$id}' should have the filename '{$file->getFilename()}'."
            );
        }

        if ($file->getType() !== FileType::Image) {
            throw new FileNotFoundException("The file with the id '{$id}' is not an image.");
        }

        return $file;
    }
}
