<?php

declare(strict_types=1);

namespace App\Application\Controller\Api;

use App\Application\Mapper\FileOutputMapper;
use App\Application\Util\CreateJsonResponse;
use App\Domain\Exception\InvalidFileInputException;
use App\Domain\Exception\InvalidFilenameException;
use App\Domain\UseCase\UploadChunckedFileUseCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;

final class ChunkedUploadController
{
    public function __construct(
        private CreateJsonResponse $createJsonResponse,
        private FileOutputMapper $fileOutputMapper,
        private UploadChunckedFileUseCase $uploadChunckedFileUseCase,
    ) {
    }

    #[Route("v1/chunk/upload", name: 'app_api_v1_chunk_upload', methods: ['POST'])]
    public function uploadFileAction(
        #[MapRequestPayload] $uploadedChunkedFileInput
    ): Response {
        return new Response('');
    }
}
