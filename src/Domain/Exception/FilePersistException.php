<?php

namespace App\Domain\Exception;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FilePersistException extends BadRequestHttpException
{
}
