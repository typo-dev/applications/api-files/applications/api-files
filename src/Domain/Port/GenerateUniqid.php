<?php

namespace App\Domain\Port;

interface GenerateUniqid
{
    public function generate(): string;
}
