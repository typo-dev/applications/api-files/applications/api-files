<?php

declare(strict_types=1);

namespace App\Domain\Input;

final class UploadedFileInput
{
    public function __construct(
        private readonly string $filename,
        private readonly string $content
    ) {
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}
