<?php

namespace App\Domain\Entity;

class UploadingFile
{
    /**
     * @param array<Chunk> $chunks
     */
    public function __construct(
        private readonly string $id,
        private readonly string $filename,
        private readonly \DateTime $startedAt,
        private readonly int $totalChunks,
        private array $chunks = []
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function getStartedAt(): \DateTime
    {
        return $this->startedAt;
    }

    public function getTotalChunks(): int
    {
        return $this->totalChunks;
    }


    public function addChunk(Chunk $chunk): self
    {
        $this->chunks[] = $chunk;
        return $this;
    }
}
