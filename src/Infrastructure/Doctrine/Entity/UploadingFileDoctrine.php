<?php

namespace App\Infrastructure\Doctrine\Entity;

use App\Domain\Entity\UploadingFile;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 't_uploading_file')]
class UploadingFileDoctrine
{
    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 36, unique: true)]
    private string $id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $filename;

    #[ORM\Column(type: 'datetime')]
    private \DateTime $startedAt;

    #[ORM\Column(type: 'integer')]
    private int $totalChunks;

    /**
     * @var ArrayCollection<ChunkDoctrine>|Collection<ChunkDoctrine> $chunks
     */
    #[ORM\OneToMany(targetEntity: ChunkDoctrine::class, mappedBy: 'uploadingFile')]
    private Collection|ArrayCollection $chunks;

    public function __construct(
        string $id,
        string $filename,
        \DateTime $startedAt,
        int $totalChunks
    ) {
        $this->id = $id;
        $this->filename = $filename;
        $this->startedAt = $startedAt;
        $this->totalChunks = $totalChunks;
        $this->chunks = new ArrayCollection();
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function getStartedAt(): \DateTime
    {
        return $this->startedAt;
    }

    public function getTotalChunks(): int
    {
        return $this->totalChunks;
    }

    /**
     * @return Collection<ChunkDoctrine>
     */
    public function getChunks(): Collection
    {
        return $this->chunks;
    }

    public function addChunk(ChunkDoctrine $chunk): self
    {
        if (!$this->chunks->contains($chunk)) {
            $this->chunks->add($chunk);
        }
        return $this;
    }

    public function toUploadingFile(): UploadingFile
    {
        return new UploadingFile(
            $this->id,
            $this->filename,
            $this->startedAt,
            $this->totalChunks,
            array_map(function (ChunkDoctrine $chunkDoctrine) {
                return $chunkDoctrine->toChunk();
            }, $this->chunks->toArray())
        );
    }
}
