<?php

declare(strict_types=1);

namespace App\Application\Controller\Api;

use App\Application\Mapper\FileOutputMapper;
use App\Application\Util\CreateJsonResponse;
use App\Domain\Exception\InvalidFileInputException;
use App\Domain\Exception\InvalidFilenameException;
use App\Domain\Input\UploadedFileInput;
use App\Domain\UseCase\UploadFileUseCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;

final class UploadController
{
    public function __construct(
        private CreateJsonResponse $createJsonResponse,
        private UploadFileUseCase $uploadFileUseCase,
        private FileOutputMapper $fileOutputMapper
    ) {
    }

    #[Route("v1/upload", name: 'app_api_v1_upload', methods: ['POST'])]
    public function uploadFileAction(
        #[MapRequestPayload] UploadedFileInput $uploadedFileInput
    ): Response {
        try {
            $file = $this->uploadFileUseCase->execute($uploadedFileInput, new \DateTime());
            $fileOutput = $this->fileOutputMapper->toFileOutput($file);

            return $this->createJsonResponse->sendSuccess(
                $fileOutput,
                'The file was uploaded successfully.'
            );
        } catch (InvalidFileInputException | InvalidFilenameException $exception) {
            return $this->createJsonResponse->sendResponse(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                null,
                $exception->getMessage()
            );
        }
    }
}
