<?php

namespace App\Domain\UseCase;

use App\Domain\Exception\FileNotFoundException;
use App\Domain\Exception\FilePersistException;
use App\Domain\Port\FilePersist;

class GetFileContentUseCase
{
    public function __construct(
        private FilePersist $filePersist,
    ) {
    }

    /**
     * @throws FilePersistException
     * @throws FileNotFoundException
     * @return resource
     */
    public function executer(string $id)
    {
        return $this->filePersist->getFileContent($id);
    }
}
