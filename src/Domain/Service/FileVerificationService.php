<?php

namespace App\Domain\Service;

use App\Domain\Exception\InvalidFilenameException;

class FileVerificationService
{
    public const FILENAME_MAX_SIZE = 250;

    /**
     * @throws InvalidFilenameException
     */
    public function validateFilename(string $filename): void
    {
        if (!$filename) {
            throw new InvalidFilenameException('The filename cannot be empty.');
        }
        if (strlen($filename) > self::FILENAME_MAX_SIZE) {
            throw new InvalidFilenameException('The filename cannot be longer than 250 characters.');
        }
    }
}
