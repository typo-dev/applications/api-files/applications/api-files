<?php

namespace App\Application\Util;

class JsonFormatReponse
{
    private mixed $data;
    private ?string $message;

    public function __construct(mixed $data = null, ?string $message = null)
    {
        $this->data = $data;
        $this->message = $message;
    }

    public function getData(): mixed
    {
        return $this->data;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }
}
