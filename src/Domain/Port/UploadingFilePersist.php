<?php

namespace App\Domain\Port;

use App\Domain\Entity\Chunk;
use App\Domain\Entity\File;
use App\Domain\Entity\UploadingFile;

interface UploadingFilePersist
{
    public function createUploadingFile(UploadingFile $uploadingFile): void;

    public function getById(string $id): UploadingFile;

    public function uploadChunk(UploadingFile $uploadingFile, Chunk $chunk, string $content): UploadingFile;

    public function mergedChunkAndCreateFile(string $idUploadingFile, string $newId): File;
}
