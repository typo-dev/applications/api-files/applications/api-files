<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241020141110 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE t_chunk (id VARCHAR(36) NOT NULL, uploading_file_id VARCHAR(36) DEFAULT NULL, number INT NOT NULL, uploaded_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, path VARCHAR(127) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2BB71757FD34D444 ON t_chunk (uploading_file_id)');
        $this->addSql('CREATE TABLE t_file (id VARCHAR(36) NOT NULL, filename VARCHAR(255) NOT NULL, mime_type VARCHAR(127) NOT NULL, path VARCHAR(127) NOT NULL, size INT NOT NULL, uploaded_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE t_optimized_file (id VARCHAR(36) NOT NULL, file_id VARCHAR(36) DEFAULT NULL, mime_type VARCHAR(127) NOT NULL, size INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E5AF6C7693CB796C ON t_optimized_file (file_id)');
        $this->addSql('CREATE TABLE t_uploading_file (id VARCHAR(36) NOT NULL, filename VARCHAR(255) NOT NULL, started_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, total_chunks INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE t_chunk ADD CONSTRAINT FK_2BB71757FD34D444 FOREIGN KEY (uploading_file_id) REFERENCES t_uploading_file (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE t_optimized_file ADD CONSTRAINT FK_E5AF6C7693CB796C FOREIGN KEY (file_id) REFERENCES t_file (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE t_chunk DROP CONSTRAINT FK_2BB71757FD34D444');
        $this->addSql('ALTER TABLE t_optimized_file DROP CONSTRAINT FK_E5AF6C7693CB796C');
        $this->addSql('DROP TABLE t_chunk');
        $this->addSql('DROP TABLE t_file');
        $this->addSql('DROP TABLE t_optimized_file');
        $this->addSql('DROP TABLE t_uploading_file');
    }
}
