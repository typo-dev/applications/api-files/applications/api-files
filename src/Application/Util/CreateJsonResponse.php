<?php

namespace App\Application\Util;

use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;

final class CreateJsonResponse
{
    public function __construct(
        private readonly SerializerInterface $serializer,
    ) {
    }

    public function sendSuccess(
        mixed $data = null,
        ?string $message = null,
        int $status = Response::HTTP_OK
    ): Response {
        return $this->sendResponse($status, $data, $message);
    }

    public function sendResponse(
        int $status,
        mixed $data = null,
        ?string $message = null,
    ): Response {
        $json = $this->serializer->serialize(
            new JsonFormatReponse($data, $message),
            'json'
        );
        return new Response($json, $status, ['Content-Type' => 'application/json']);
    }
}
