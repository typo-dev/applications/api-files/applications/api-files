<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum FileType: string
{
    case Image = 'image';
    case Audio = 'audio';
    case Other = 'other';
}
