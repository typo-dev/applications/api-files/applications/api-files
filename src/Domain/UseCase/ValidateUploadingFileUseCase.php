<?php

namespace App\Domain\UseCase;

use App\Domain\Entity\File;
use App\Domain\Port\GenerateUniqid;
use App\Domain\Port\UploadingFilePersist;

class ValidateUploadingFileUseCase
{
    public function __construct(
        private readonly GenerateUniqid $generateUniqid,
        private readonly UploadingFilePersist $uploadingFilePersist,
    ) {
    }

    public function execute(string $idUploadingFile): File
    {
        return $this->uploadingFilePersist->mergedChunkAndCreateFile(
            $idUploadingFile,
            $this->generateUniqid->generate()
        );
    }
}
